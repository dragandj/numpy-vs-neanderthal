# Numpy benchmarks

This is a little collection of [Numpy](http://www.numpy.org/) benchmarks aimed at comparing it with [Neanderthal](https://neanderthal.uncomplicate.org/). These benchmarks were developed for [this series](https://dragan.rocks/articles/18/Neanderthal-vs-ND4J-vol1) of blog posts.

## Running with Docker

This is the preferred way to run the benchmarks: with the Docker container you get a fully working environment with [MKL](https://software.intel.com/en-us/mkl) already installed. 

To run the benchmarks without modifications just do:

```bash
git clone https://gitlab.com/alanmarazzi/numpy-vs-neanderthal.git
cd numpy-vs-neanderthal
docker build -t numpy-bench .
docker run --rm numpy-bench
```

This will run benchmarks and print the result to the shell.

### Too slow?

Benchmarks are pretty slow, if you have issues to finish running them you can modify the `run_bench.sh` file to perform less loops:

```bash
python numpy_bench.py \
  #--fast # This is the default option, remove it
  -p 1 \ # Add -p and -n options (n° loops = p*n)
  -n 1 \
  -o "bench.json" && \
  python -m perf \
         stats bench.json
```

## Store results

If you'd like to store the results JSON, you can just mount this repo as a volume when running the container:

```bash
docker run --rm -v {$pwd}:/benchmarks -t numpy-bench
```

At the end of the process you'll find a `bench.json` file in your folder.

## Neanderthal code

Refer to [README](https://gitlab.com/alanmarazzi/numpy-vs-neanderthal/blob/master/neanderthal-bench/README.md) inside `neanderthal-bench` project.

# Results

Benchmark results on my **Intel Core i7-6700HQ CPU @ 2.60GHz × 4** for both **Numpy** and **Neanderthal**.

## Summary

```
MM 4x4
=========

Numpy:       744 ns +- 52 ns
Neanderthal: 705 ns +- 12 ns

MM 128x128
==========

Numpy:       19 µs +- 2.8 µs
Neanderthal: 16 µs +- 3 µs

MM 1024x1024
===========

Numpy:       7.91 ms +- 2.29 ms
Neanderthal: 7.34 ms +- 1.28 ms

MM 4096x4096
===========

Numpy:       412 ms +- 18 ms
Neanderthal: 462.76 ms +- 12.87 ms

MM 10000x10000
===========

Numpy:       6.08 sec +- 0.09 sec
Neanderthal: 6.04 sec +- 69.57 ms

SVD 4x4
===========

Numpy:             17.5 µs +- 0.7 µs
Neanderthal:       4.23 µs +- 75.38 ns
Neanderthal (SDD): 3.8 µs +- 101.37 ns

SVD 128x128
===========

Numpy:             1.99 ms +- 0.02 ms
Neanderthal:       838.47 µs +- 46.48 µs
Neanderthal (SDD): 811.47 µs +- 17.9 ns

SVD 1024x1024
===========

Numpy:             292 ms +- 13 ms
Neanderthal:       161.06 ms +- 5.28 ms
Neanderthal (SDD): 194.48 ms +- 11.17 ms

SVD 4096x4096
===========

Numpy:             22.7 sec +- 2.8 sec
Neanderthal:       5.84 sec +- 54.56 ms
Neanderthal (SDD): 10.85 sec +- 125.02 ms

PCA 4x4
===========

Numpy (eigh):       90.3 µs +- 1.9 µs
Numpy (eig):        98.2 µs +- 2.3 µs
Neanderthal (es!):  37 µs +- 1.02 µs
Neanderthal (ev!):  45.5 µs +- 1.92 µs

PCA 128x128
===========

Numpy (eigh):       1.28 ms +- 0.02 ms
Numpy (eig):        5.61 ms +- 0.08 ms
Neanderthal (es!):  3.49 ms +- 7.5 µs
Neanderthal (ev!):  4.89 ms +- 289 µs

PCA 1024x1024
===========

Numpy (eigh):       161 ms +- 18 ms
Numpy (eig):        687 ms +- 16 ms
Neanderthal (es!):  509 ms +- 15 ms
Neanderthal (ev!):  711.6 ms +- 14 ms

PCA 4096x4096
===========

Numpy (eigh):       11.5 sec +- 0.1 sec
Numpy (eig):        39.8 sec +- 4.3 sec
Neanderthal (es!):  34.93 sec +- 694.25 ms
Neanderthal (ev!):  43.28 sec +- 47.39 ms

SME 4x4 (Symmetric Matrix Eigenvalues)
===========

Numpy:       23.5 µs +- 2.4 µs
Neanderthal: 5 µs +- 105 ns

SME 128x128 (Symmetric Matrix Eigenvalues)
===========

Numpy:       919 µs +- 12 µs
Neanderthal: 303 µs +- 4.6 µs

SME 1024x1024 (Symmetric Matrix Eigenvalues)
===========

Numpy:       92.8 ms +- 1.9 ms
Neanderthal: 139.33 ms +- 10.27 ms

SME 4096x4096 (Symmetric Matrix Eigenvalues)
===========

Numpy:       7.76 sec +- 0.03 sec
Neanderthal: 3.72 sec +- 4.62 sec

```

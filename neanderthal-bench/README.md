# neanderthal-bench

## Running benchmarks

As for **Numpy** you can run via Docker without bothering with installations.

```bash
cd neanderthal-bench
docker build -t neandertal-bench .
docker run --rm -t neanderthal-bench
```

While building if you see that the process stops at `done`, don't worry, it takes a lot of time, but eventually it will finish.

(ns neanderthal-bench.core
  (:require [uncomplicate.commons.core :refer [with-release
                                               let-release
                                               release
                                               double-fn]]
            [uncomplicate.fluokitten.core :refer [fmap!
                                                  fmap
                                                  foldmap
                                                  fold]]
            [uncomplicate.neanderthal
             [aux :refer :all]
             [core :refer :all]
             [native :refer :all]
             [linalg :refer :all]
             [vect-math :refer :all]]
            [criterium.core :refer [quick-bench
                                    report]])
  (:import java.util.SplittableRandom))

(let [splittable-random (SplittableRandom.)]
  (defn random ^double [^double _]
    (.nextDouble ^SplittableRandom splittable-random)))

(defn bench-neanderthal-mm!-float [^long m ^long k ^long n]
  (with-release [m1 (fmap! random (fge m k))
                 m2 (fmap! random (fge k n))
                 result (fge m n)]
    (quick-bench 
      (do (mm! 1.0 m1 m2 0.0 result)
          true))))

(defn bench-svd-sdd [^long m]
  (with-release [m1 (fmap! random (fge m m))]
    (quick-bench 
      (do (svd! m1 (fgd m))
          true))))

(defn bench-svd [^long m]
  (with-release [m1 (fmap! random (fge m m))]
    (quick-bench 
      (do (svd! m1
                (fgd m)
                (fgd m))
          true))))

(defn matrix-mean! [a ones res]
  (if (and (< 0 (dim a)))
    (mv! (/ 1.0 (ncols a)) a ones 0.0 res)
    (entry! res Double/NaN)))

(defn column-means
  [m]
  (let-release [res (vctr m (ncols m))]
    (with-release [ones (entry! (raw (col m 0)) 1.0)]
      (matrix-mean! (trans m) ones res))))

(defn center
  [m]
  (with-release [means (column-means m)]
    (axpy! -1
           (rk! (entry! (raw (row m 0)) 1.0) means (fge (mrows m) (ncols m)))
           (copy m))))

(defn cov
  [m]
  (let-release [centered  (center m)
                cols      (ncols m)
                result    (fge cols cols)]
    (scal! (/ 1 (- cols 1))
           (mm! 1.0 (trans centered) centered 1.0 result))))

(defn pca
  [m n-components]
  (with-release [m (fmap! random (fge m m))]
    (quick-bench
      (let-release [rows       (mrows m)
                    cols       (ncols m)
                    centered   (center m)
                    covariance (cov centered)
                    evals      (fge rows 2)
                    evecs      (fge rows cols)
                    qr         (es! (copy covariance) evals evecs)
                    top        (submatrix evecs rows n-components)
                    result     (fge rows n-components)]
        (mm! 1.0 covariance top 0.0 result)))))

(defn pca-ev
  [m n-components]
  (with-release [m (fmap! random (fge m m))]
    (quick-bench
      (let-release [rows       (mrows m)
                    cols       (ncols m)
                    centered   (center m)
                    covariance (cov centered)
                    evecs      (fge rows cols)
                    qr         (ev! (copy covariance) (fge rows 2)
                                    evecs evecs)
                    top        (submatrix evecs rows n-components)
                    result     (fge rows n-components)]
        (mm! 1.0 covariance top 0.0 result)))))

(defn bench-symmetric-ev! [^long m]
  (with-release [m1 (fmap! random (fsy m))]
    (quick-bench 
      (do (ev! m1 (fge m 1))
          true))))

(defn -main
  []
  (do
    (println "Neanderthal Bench\n===========")
    (println "\nVol_1 4x4")
    (bench-neanderthal-mm!-float 4 4 4)
    (println "\nVol_1 128x128")
    (bench-neanderthal-mm!-float 128 128 128)
    (println "\nVol_1 1024x1024")
    (bench-neanderthal-mm!-float 1024 1024 1024)
    (println "\nVol_1 4096x4096")
    (bench-neanderthal-mm!-float 4096 4096 4096)
    (println "\nVol_1 10000x10000")
    (bench-neanderthal-mm!-float 10000 10000 10000)
    (println "\nSVD 4x4")
    (bench-svd 4)
    (println "\nSVD 128x128")
    (bench-svd 128)
    (println "\nSVD 1024x1024")
    (bench-svd 1024)
    (println "\nSVD 4096x4096")
    (bench-svd 4096)
    (println "\nSVD-SDD 4x4")
    (bench-svd-sdd 4)
    (println "\nSVD-SDD 128x128")
    (bench-svd-sdd 128)
    (println "\nSVD-SDD 1024x1024")
    (bench-svd-sdd 1024)
    (println "\nSVD-SDD 4096x4096")
    (bench-svd-sdd 4096)
    (println "\nPCA 4x4")
    (pca 4 2)
    (println "\nPCA 128x128")
    (pca 128 2)
    (println "\nPCA 1024x1024")
    (pca 1024 2)
    (println "\nPCA 4096x4096")
    (pca 4096 2)
    (println "\nPCA-EV 4x4")
    (pca-ev 4 2)
    (println "\nPCA-EV 128x128")
    (pca-ev 128 2)
    (println "\nPCA-EV 1024x1024")
    (pca-ev 1024 2)
    (println "\nPCA-EV 4096x4096")
    (pca-ev 4096 2)
    (println "\nEV-SY 4x4")
    (bench-symmetric-ev! 4)
    (println "\nEV-SY 128x128")
    (bench-symmetric-ev! 128)
    (println "\nEV-SY 1024x1024")
    (bench-symmetric-ev! 1024)
    (println "\nEV-SY 4096x4096")
    (bench-symmetric-ev! 4096)))

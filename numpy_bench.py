import numpy as np
import perf

runner = perf.Runner()
setup = "from __main__ import matmul, m4, m128, m1024, m4096, m10000, pca, svd, bench_eigh"

def make_matrix(m, k, n):
    m1 = np.random.rand(m, k).astype(np.float32)
    m2 = np.random.rand(k, n).astype(np.float32)
    result = np.empty([m, n]).astype(np.float32)
    return m1, m2, result

m4 = make_matrix(4, 4, 4)
m128 = make_matrix(128, 128, 128)
m1024 = make_matrix(1024, 1024, 1024)
m4096 = make_matrix(4096, 4096, 4096)
m10000 = make_matrix(10000, 10000, 10000)

def matmul(m1, m2, result):
    return np.matmul(m1, m2, result)

def svd(m):
    return np.linalg.svd(m)

def pca(mat, n_components=2):
    x = mat - np.mean(mat, axis=0)
    cov = np.cov(x, rowvar=False)
    evals, evecs = np.linalg.eig(cov)
    idx = np.argsort(evals)[::-1]
    evals = evals[idx]
    evecs = evecs[:, idx]
    evecs = evecs[:, :n_components]

    return np.dot(evecs.T, cov.T).T

def bench_eigh(mat):
    return np.linalg.eigh(mat)


runner.timeit("mmult_float_4x4", "matmul(*m4)",
              setup=setup)

runner.timeit("mmult_float_128x128", "matmul(*m128)",
              setup=setup)

runner.timeit("mmult_float_1024x1024", "matmul(*m1024)",
              setup=setup)

runner.timeit("mmult_float_4096x4096", "matmul(*m4096)",
              setup=setup)

runner.timeit("mmult_float_10000x10000", "matmul(*m10000)",
              setup=setup)

runner.timeit("svd 4x4", "svd(m4[0])", setup=setup)

runner.timeit("svd 128x128", "svd(m128[0])", setup=setup)

runner.timeit("svd 1024x1024", "svd(m1024[0])", setup=setup)

runner.timeit("svd 4096x4096", "svd(m4096[0])", setup=setup)

runner.timeit("pca 4x4", "pca(m4[0], 2)", setup=setup)

runner.timeit("pca 128x128", "pca(m128[0], 2)", setup=setup)

runner.timeit("pca 1024x1024", "pca(m1024[0], 2)", setup=setup)

runner.timeit("pca 4096x4096", "pca(m4096[0], 2)", setup=setup)

runner.timeit("eigh 4x4", "bench_eigh(m4[0])", setup=setup)

runner.timeit("eigh 128x128", "bench_eigh(m128[0])", setup=setup)

runner.timeit("eigh 1024x1024", "bench_eigh(m1024[0])", setup=setup)

runner.timeit("eigh 4096x4096", "bench_eigh(m4096[0])", setup=setup)

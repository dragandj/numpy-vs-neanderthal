FROM continuumio/miniconda3

WORKDIR /benchmarks

RUN apt-get update && apt-get -y upgrade

RUN pip install perf

ADD . /benchmarks

RUN conda install -y --file specs.txt

CMD ["bash", "run_bench.sh"]
